package lab02jam;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;

public class ClassInfo {
    private Class lClass = null;
	private String className;
    private String[] superClasses;
    private int classModifiers;
    private Class[] interfaces;
    private Field[] fields;
    private Constructor[] constuctors;
    private Method[] methods;
    private Object classObject;
    ClassInfo(File file, String name){
    	URL url;
		try {
			url = file.toURI().toURL();
	        URL[] urls = new URL[]{url};
	    	ClassLoader loader = new URLClassLoader(urls);
	    	this.className=name;
			try {
				lClass = loader.loadClass(name);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	fields = lClass.getFields();
    	methods = lClass.getMethods();
    	constuctors = lClass.getConstructors();
    	try {
			classObject = constuctors[0].newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	this.Frame(name);
    }
    public void  Frame(String name){
		 JFrame frame = new JFrame(name);
	      JPanel contentPane = new JPanel();
	        contentPane.setOpaque(true);
	        contentPane.setLayout(null);

	        JList listV=  new JList(this.classFields());
	        listV.setVisibleRowCount(4);
	        listV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        listV.setSize(270,300);
	        listV.setLocation(370, 10);
	        JList listM =  new JList(this.classMethods());
	        listM.setVisibleRowCount(4);
	        listM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        listM.setSize(170,200);
	        listM.setLocation(30, 10);
	        
	        JTextArea textArea1 = new JTextArea();
	        textArea1.setSize(100,20);
	        textArea1.setLocation(215,100);

	        JButton addTask = new JButton("wykonaj funkcje");
	        addTask.setSize(100, 50);
	        addTask.setLocation(215, 15);
	        
	        JButton changeField = new JButton("zmien wartosc");
	        changeField.setSize(100, 50);
	        changeField.setLocation(215, 125);
	        
	        contentPane.add(listV);
	        contentPane.add(listM);
	        contentPane.add(addTask);
	        contentPane.add(changeField);
	        contentPane.add(textArea1);
	        frame.setContentPane(contentPane);
	        frame.setSize(710, 425);
	        frame.setLocationByPlatform(true);
	        frame.setVisible(true);
	        frame.addWindowListener(new WindowAdapter()
	        {
	            public void windowClosing(WindowEvent e)
	            {
	                lClass = null;
	                interfaces = null;
	                fields = null;
	                constuctors = null;
	                methods = null;
	                classObject = null;
	            }
	        });

	        addTask.addActionListener(ae -> {
                Class[] parameters = methods[listM.getSelectedIndex()].getParameterTypes();
		        ArrayList<Object> objectList = new ArrayList<Object>();
		        int i = 1;
                for (Class pam : parameters) {
                    String typeName = pam.getTypeName();
                    Object method = null;
                    if (typeName == String.class.getTypeName()) {
                    	method = JOptionPane.showInputDialog("podaj wartosc "+ i +" typu:"+ pam.getTypeName());
                    } else if (typeName == int.class.getTypeName()) {
                    	method= Integer.parseInt(JOptionPane.showInputDialog("podaj wartosc "+ i +" typu:"+ pam.getTypeName()));
                    } else if (typeName == float.class.getTypeName()) {
                    	method = Float.parseFloat(JOptionPane.showInputDialog("podaj wartosc "+ i +" typu:"+ pam.getTypeName()));;
                    } else if (typeName == double.class.getTypeName()) {
                    	method = Double.parseDouble(JOptionPane.showInputDialog("podaj wartosc "+ i +" typu:"+ pam.getTypeName()));;
                    }
                    objectList.add(method);
                }
                Object[] arguments = objectList.toArray();
                try {
					methods[listM.getSelectedIndex()].invoke(classObject, arguments);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	
	        });
	        
	        changeField.addActionListener(ae -> {
		        Field tmp = fields[listV.getSelectedIndex()];
		        Class fieldClass = tmp.getType();
		        ArrayList<Object> objectList = new ArrayList<Object>();
		        String type = fieldClass.getTypeName();
		        Object newV = null;
		        if (!fieldClass.isArray()) {
                    if (type == String.class.getTypeName()) {
                    	newV = textArea1.getText();
                    	System.out.println("jestem tu");
                    }else if(type == int.class.getTypeName()){
                    	newV = Integer.parseInt(textArea1.getText());
                    	System.out.println("jeste");
                    }else if(type == float.class.getTypeName()){
                    	newV = Float.parseFloat(textArea1.getText());
                    }else if(type == double.class.getTypeName()){
                    	newV = Double.parseDouble(textArea1.getText());
                    }
                    try {
						fields[listV.getSelectedIndex()].set(classObject ,newV);
						listV.setListData(this.classFields());
						textArea1.setText(null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					};
		        }else{

		        	int length = Array.getLength(fields[listV.getSelectedIndex()]);
                    Class component = fieldClass.getComponentType();
                    String componentName = component.getTypeName();
                    for (int i = 0; i < length; i++) {
                        if (componentName == String.class.getTypeName()) {
                        	newV = JOptionPane.showInputDialog(frame, "podaj wartosc "+i);
                        } else if (componentName == int.class.getTypeName()) {
                        	newV = Integer.parseInt(JOptionPane.showInputDialog(frame, "podaj wartosc "+i));
                        } else if (componentName == float.class.getTypeName()) {
                        	newV = Float.parseFloat(JOptionPane.showInputDialog(frame, "podaj wartosc "+i));
                        } else if (componentName == double.class.getTypeName()) {
                        	newV = Double.parseDouble(JOptionPane.showInputDialog(frame, "podaj wartosc "+i));
                        }
                        objectList.add(newV);
                    }
                    
                    Object[] values = objectList.toArray();
					try {
						fields[listV.getSelectedIndex()].set(classObject ,values);
						listV.setListData(this.classFields());
						textArea1.setText(null);


					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		        }
	        	
			  });
    }
 
    public String[] classFields()  {
        String[] Infos = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            String fieldName = fields[i].getName();
            Class typeClass = fields[i].getType();
            String fieldType = typeClass.getSimpleName();
            String value;
			try {
				if(fields[i].get(classObject)==null){
				 Infos[i] = "Name: " + fieldName + ", Type: " + fieldType;
			}else{
				value = fields[i].get(classObject).toString();
	            Infos[i] = "Name: " + fieldName + ", Type: " + fieldType+ " Value: "+value;
			}
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return Infos;
    }
    public String[] classMethods() {
        String[] Infos = new String[methods.length];
        for (int i = 0; i < methods.length; i++) {
            String methodString = methods[i].getName();
            String returnString = methods[i].getReturnType().getSimpleName();
            Class[] parameterTypes = methods[i].getParameterTypes();
            String parameterString = "(";
            for (int k = 0; k < parameterTypes.length; k++) {
                if (k != 0) {
                    parameterString += ", ";
                }
                parameterString += parameterTypes[k].getSimpleName();
            }
            parameterString += ")";
            String method = returnString + " " + methodString + parameterString;
            Infos[i] = method;
        }
        return Infos;
    }
    public Class getlClass() {
		return lClass;
	}
	public void setlClass(Class lClass) {
		this.lClass = lClass;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String[] getSuperClasses() {
		return superClasses;
	}
	public void setSuperClasses(String[] superClasses) {
		this.superClasses = superClasses;
	}
	public int getClassModifiers() {
		return classModifiers;
	}
	public void setClassModifiers(int classModifiers) {
		this.classModifiers = classModifiers;
	}
	public Class[] getInterfaces() {
		return interfaces;
	}
	public void setInterfaces(Class[] interfaces) {
		this.interfaces = interfaces;
	}
	public Field[] getPublicFields() {
		return fields;
	}
	public void setPublicFields(Field[] publicFields) {
		this.fields = publicFields;
	}
	public Constructor[] getConstuctors() {
		return constuctors;
	}
	public void setConstuctors(Constructor[] constuctors) {
		this.constuctors = constuctors;
	}
	public Method[] getMethods() {
		return methods;
	}
	public void setMethods(Method[] methods) {
		this.methods = methods;
	}

}
